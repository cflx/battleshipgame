﻿using System.Collections.Generic;
using System.Text.Json;

namespace BattleShips
{
    public class GameRules
    {
        public int BoardSizeX { get; set; } = 10;
        public int BoardSizeY { get; set; } = 10;
        public EShipTouchRule EShipTouchRule { get; set; } = EShipTouchRule.Side;
        
        public List<ShipConfig> ShipConfigs { get; set; } = new List<ShipConfig>()
        {
            new()
            {
                Name = "Patrol",
                Quantity = 5,
                SizeY = 1,
                SizeX = 1
            },
            new()
            {
                Name = "Cruiser",
                Quantity = 4,
                SizeY = 1,
                SizeX = 2
            },
            new()
            {
                Name = "Submarine",
                Quantity = 3,
                SizeY = 1,
                SizeX = 3
            },
            new()
            {
                Name = "Battleship",
                Quantity = 2,
                SizeY = 1,
                SizeX = 4
            },
            new()
            {
                Name = "Carrier",
                Quantity = 1,
                SizeY = 1,
                SizeX = 5
            },
        };

        public int GetTotalShipCount()
        {
            int count = 0;
            foreach (var shipConf in ShipConfigs)
                count += shipConf.Quantity!.Value;

            return count;
        }
        
        public string Serialize()
        {
            JsonSerializerOptions jsonOptions = new JsonSerializerOptions() { WriteIndented = false };
            return JsonSerializer.Serialize(this, jsonOptions);
        }
    }
}