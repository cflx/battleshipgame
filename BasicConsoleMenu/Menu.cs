using System;
using System.Collections.Generic;

namespace BasicConsoleMenu
{
    public class Menu
    {
        private readonly EMenuLevel _menuLevel;
        
        private readonly List<MenuItem> _menuInfoItems = new List<MenuItem>();
        private readonly List<MenuItem> _menuMainItems = new List<MenuItem>();
        private readonly List<MenuItem> _menuNavItems = new List<MenuItem>();
        private readonly List<MenuItem> _menuTaskItems = new List<MenuItem>();
        
        private readonly MenuItem _menuItemExit = new MenuItem("Exit Program", null, EMenuItemType.Nav);
        private readonly MenuItem _menuItemReturn = new MenuItem("Return to Last Menu", null, EMenuItemType.Nav);
        private readonly MenuItem _menuItemMain = new MenuItem("Return to Main Menu", null, EMenuItemType.Nav);
        
        private readonly string _title;
        private int _selectedItemIn;
        private int _selectedItemLn;
        
        public Menu(string title, EMenuLevel menuLevel)
        {
            _title = title;
            _menuLevel = menuLevel;
            _selectedItemIn = 0;
            
            switch (_menuLevel)
            {
                case EMenuLevel.Root:
                    _menuNavItems.Add(_menuItemExit);
                    break;
                case EMenuLevel.First:
                    _menuNavItems.Add(_menuItemReturn);
                    break;
                case EMenuLevel.SecondOrMore:
                    _menuNavItems.Add(_menuItemReturn);
                    _menuNavItems.Add(_menuItemMain);
                    break;
            }
        }

        public void AddMenuItem(MenuItem item, int position = -1)
        {
            switch (item.MenuItemType)
            {
                case EMenuItemType.Info:
                    if (position == -1)
                        _menuInfoItems.Add(item);
                    else 
                        _menuInfoItems.Insert(position, item);
                    break;
                case EMenuItemType.Submenu:
                    if (position == -1)
                        _menuMainItems.Add(item);
                    else 
                        _menuMainItems.Insert(position, item);
                    break;
                case EMenuItemType.Nav:
                    if (position == -1)
                        _menuNavItems.Add(item);
                    else 
                        _menuNavItems.Insert(position, item);
                    break;
                case EMenuItemType.Task:
                    _menuTaskItems.Add(item);
                    break;
            }
        }

        public void DeleteMenuItem(string title, EMenuItemType type)
        {
            List<MenuItem> items;
            if (type == EMenuItemType.Info)
                items = _menuInfoItems;
            else if (type == EMenuItemType.Submenu)
                items = _menuMainItems;
            else // cant delete nav items
                return;

            foreach (var menuItem in items)
                if (menuItem.Title == title)
                {
                    items.Remove(menuItem);
                    return;
                }
        }

        public void DeleteMenuMainItems()
        {
            int itemCount = _menuMainItems.Count;
            for (int i = 0; i < itemCount; i++)
                _menuMainItems.RemoveAt(0);
        }
        
        public void DeleteMenuInfoItems()
        {
            int itemCount = _menuInfoItems.Count;
            for (int i = 0; i < itemCount; i++)
                _menuInfoItems.RemoveAt(0);
        }
        
        public void AddMenuItems(List<MenuItem> items)
        {
            foreach (var menuItem in items)
                AddMenuItem(menuItem);
        }

        private List<MenuItem> GetSelectableMenuItems()
        {
            List<MenuItem> allMenuItems = new List<MenuItem>();

            foreach (var menuItem in _menuMainItems)
                allMenuItems.Add(menuItem);
            
            foreach (var menuItem in _menuNavItems)
                allMenuItems.Add(menuItem);
            
            return allMenuItems;
        }
        
        private void SwitchSelectedItem(in List<MenuItem> allMenuItems, int direction)
        {
            _selectedItemIn += direction;
            
            Console.SetCursorPosition(0, Console.GetCursorPosition().Top - 1);

            if (direction == -1) // up
            {
                Console.WriteLine(allMenuItems[_selectedItemIn + 1]);
                if (_selectedItemIn != _menuMainItems.Count - 1)
                    _selectedItemLn--;
                else
                    _selectedItemLn -= 2; // move 2 lines to skip the seperator
            }
            else // down
            {
                Console.WriteLine(allMenuItems[_selectedItemIn - 1]);
                if (_selectedItemIn != _menuMainItems.Count)
                    _selectedItemLn++;
                else
                    _selectedItemLn += 2;
            }
        }

        public string Run()
        {
            OutputMenu();
            
            bool runDone = false;
            do
            {
                // updated every time so upon adding/removing items menu stays functional
                List<MenuItem> allMenuItems = GetSelectableMenuItems();
                Console.SetCursorPosition(0, _selectedItemLn);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(allMenuItems[_selectedItemIn]);
                Console.ResetColor();

                ConsoleKey key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        if (_selectedItemIn != 0)
                            SwitchSelectedItem(allMenuItems, -1);
                        break;
                    case ConsoleKey.DownArrow:
                        if (_selectedItemIn != allMenuItems.Count - 1)
                            SwitchSelectedItem(allMenuItems, 1);
                        break;
                    case ConsoleKey.Enter:
                        // clear old scene as pressing enter (almost) always implies moving to a new menu
                        // or a change in the scene
                        Console.Clear();

                        if (allMenuItems[_selectedItemIn].RunMethod == null)
                        {
                            if (allMenuItems[_selectedItemIn] == _menuItemExit)
                                Environment.Exit(0);
                            if (allMenuItems[_selectedItemIn] == _menuItemMain)
                                return "root";
                            runDone = true;
                            break;
                        }
                        var end = allMenuItems[_selectedItemIn].RunMethod!();

                        if (end == "return" && (_menuLevel != EMenuLevel.Root))
                            return "";

                        if (end == "root" && (_menuLevel != EMenuLevel.Root))
                            return "root";

                        if (end != "" && (_menuLevel != EMenuLevel.Root))
                            return end;

                        OutputMenu();
                        break;
                    }
            } while (!runDone);
            
            return "";
        }

        private int GetSelectedItemLn()
        {
            int ln = Console.GetCursorPosition().Top - (_selectedItemIn >= _menuMainItems.Count ? 1 : 2);
            return ln - (GetSelectableMenuItems().Count - _selectedItemIn);
        }

        private void OutputMenu()
        {
            // clear again upon returning from new scene
            Console.Clear();
            
            foreach (var menuItem in _menuTaskItems)
                menuItem.Invoke();

            Console.WriteLine("====|" + _title + "|====");
            
            foreach (var menuItem in _menuInfoItems)
                Console.WriteLine(menuItem.GetInfo());

            if (_menuMainItems.Count != 0)
                Console.WriteLine("-------------------");
            
            List<MenuItem> allMenuItems = GetSelectableMenuItems();
            for (var i = 0; i < allMenuItems.Count; i++) 
            {
                if (i == _menuMainItems.Count)
                    Console.WriteLine("-------------------");

                Console.WriteLine(allMenuItems[i]);
            }

            Console.WriteLine("=====================");
            _selectedItemLn = GetSelectedItemLn();
        }
    }
}