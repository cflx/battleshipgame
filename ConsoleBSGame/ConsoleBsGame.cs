﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Text.Json;
using BasicConsoleMenu;
using BattleShips;
using DAL;
using Domain;
using static ConsoleBSGame.Utils;
using GameState = BattleShips.GameState;

namespace ConsoleBSGame
{
    class ConsoleBsGame
    {
        public ConfigDao ConfigDao = default!;
        public GameDao GameDao = default!;

        static void Main()
        {
            Console.CursorVisible = false;
            ConsoleBsGame consoleBsGame = new ConsoleBsGame();
            consoleBsGame.Start();            
        }
        
        public void Start()
        {
            ConfigDao = new ConfigDao();
            GameDao = new GameDao();
            
            Menu menu = new Menu("Main Menu", EMenuLevel.Root);
            
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("New Game (using default parameters)", () => NewBsGameMenu(ConfigDao.GetConfigLocal("default")), EMenuItemType.Submenu),
                new MenuItem("Browse Games", GamesMenu, EMenuItemType.Submenu),
                new MenuItem("Browse Configs", ConfigsMenu, EMenuItemType.Submenu),
            });
            
            menu.Run();
        }
        
        private string GamesMenu()
        {
            Menu menu = new Menu("Main Menu | Games", EMenuLevel.First);
            
            List<MenuItem> savedGames = new();

            if (!GetBoolInput("Browse Local (0) or MP (1) games"))
            {
                foreach (var saveFile in GameDao.ListLocalGames())
                    savedGames.Add(new MenuItem("[LOC]" + Path.GetFileName(saveFile), () =>
                        LocalGameActionsMenu(GameDao.LoadGameLocal(saveFile), Path.GetFileName(saveFile)), EMenuItemType.Submenu));
            }
            else
            {
                string name = GetStringInput("Enter your name", "[^a-zA-Z0-9]", 16);

                foreach (var game in GameDao.ListDbGamesWithPlayer(name))
                    savedGames.Add(new MenuItem("[DB]" + game.GameName!, () =>
                        MpGameActionsMenu(game.GameId, name), EMenuItemType.Submenu));
            }

            menu.AddMenuItems(savedGames);
            return menu.Run();
        }

        private string LocalGameActionsMenu(GameDto gameDto, string fileName)
        {
            Menu menu = new Menu("Main Menu | Games | Actions", EMenuLevel.SecondOrMore);

            BattleShipGame bsGame = new BattleShipGame(gameDto);
            
            menu.AddMenuItem(new MenuItem("", () =>
            {
                menu.DeleteMenuMainItems();
                menu.DeleteMenuInfoItems();

                menu.AddMenuItems(new List<MenuItem>()
                {
                    new MenuItem("State: ", () => bsGame.ActiveGameState!.Describe(), EMenuItemType.Info),
                    new MenuItem("Start", () => BsGameLocal(bsGame), EMenuItemType.Submenu),
                    new MenuItem("Delete", () =>
                    {
                        if (GetBoolInput("Are you sure you want to delete this game!"))
                            GameDao.DeleteLocalByName(fileName);
                        return "root";
                    }, EMenuItemType.Submenu)

                });
            
                if (bsGame.Finished) 
                    menu.AddMenuItem(new MenuItem("View Past Gamestates", () =>
                    {
                        GameState? newActiveGameState;
                        if (GameStateMenu(bsGame.GameStates, out newActiveGameState) == "setstate")
                        {
                            bsGame.PushGameState(new GameState(newActiveGameState));
                            GameDao.SaveGameLocal(fileName, bsGame.CreateDto());
                        }
                        
                    }, EMenuItemType.Submenu));
            }, EMenuItemType.Task));
            
            return menu.Run();
        }

        private string MpGameActionsMenu(int gameId, string playerName)
        {
            Menu menu = new Menu("Main Menu | Games | Actions", EMenuLevel.SecondOrMore);
            
            Game gameDbEntry = GameDao.GetDbGameById(gameId);
            BattleShipGame bsGame = default!;
            
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("", () => bsGame = new BattleShipGame(gameDbEntry), EMenuItemType.Task),
                new MenuItem("State: ", () => bsGame.ActiveGameState!.Describe(), EMenuItemType.Info),
                new MenuItem("Start", () => BsGameMultiPlayer(gameDbEntry.GameId, playerName), EMenuItemType.Submenu),
                new MenuItem("Delete", () =>
                {
                    if (GetBoolInput("Are you sure you want to delete this game!"))
                        GameDao.DeleteDbGameById(gameDbEntry.GameId);
                    return "root";
                }, EMenuItemType.Submenu)
            });
            
            if (gameDbEntry.Finished) 
                menu.AddMenuItem(new MenuItem("View Past Gamestates", () =>
                {
                    GameState? newActiveGameState;
                    List<GameState> states = new List<GameState>();

                    foreach (var dbState in gameDbEntry.GameStates!)
                    {
                        GameState state = new GameState(
                            JsonSerializer.Deserialize<GameDto.GameStateDto>(dbState.GameStateJson)!);
                        if (state.Player1 != null && state.Player2 != null)
                            states.Add(state);
                    }                    
                    if (GameStateMenu(states, out newActiveGameState) == "setstate")
                        GameDao.PushGameStateDb(gameDbEntry.GameId, newActiveGameState!.CreateDto());
                }, EMenuItemType.Submenu));

            return menu.Run();
        }
        
        private string GameStateMenu(List<GameState> states, out GameState? newActiveState)
        {
            Menu menu = new Menu("Main Menu | Games | Actions | Gamestates", EMenuLevel.First);
            
            // view last state by default
            int selectedStateIn = states.Count - 1;
            
            menu.AddMenuItem(new MenuItem("", () =>
            {
                menu.DeleteMenuMainItems();
                menu.DeleteMenuInfoItems();

                menu.AddMenuItems(new List<MenuItem>()
                {
                    new MenuItem("", () => PrintState(states[selectedStateIn], true), EMenuItemType.Info),
                    new MenuItem("State: ", () => states[selectedStateIn].Describe(), EMenuItemType.Info),
                    new MenuItem("Created: ", () => states[selectedStateIn].DateTime.ToString(CultureInfo.CurrentCulture), EMenuItemType.Info),
                    new MenuItem("Index: ", () => (selectedStateIn + 1).ToString(), EMenuItemType.Info),
                    new MenuItem("Set As Activestate", () => "setstate", EMenuItemType.Submenu)
                });
                
                if (selectedStateIn != 0)
                    menu.AddMenuItem(new MenuItem("Previous", () => { selectedStateIn--; }, EMenuItemType.Submenu));
                
                if (selectedStateIn < states.Count - 1)
                    menu.AddMenuItem(new MenuItem("Next", () => { selectedStateIn++; }, EMenuItemType.Submenu));
                
            },EMenuItemType.Task));

            string result = menu.Run();

            if (result == "setstate")
                if (selectedStateIn != states.Count - 1)
                {
                    newActiveState = states[selectedStateIn];
                    return result;
                }

            newActiveState = null;
            return result;
        }
        
        private string ConfigsMenu()
        {
            Menu menu = new Menu("Main Menu | Configs", EMenuLevel.First);
            List<MenuItem> menuOptions = new List<MenuItem>();
            
            menu.AddMenuItem(new MenuItem("Create New", () => ConfigActionsMenu(null, null, null), EMenuItemType.Submenu));

            foreach (var configPath in ConfigDao.ListLocalConfigs())
                menuOptions.Add(new MenuItem("[LOC]" + Path.GetFileName(configPath), 
                    () => ConfigActionsMenu(Path.GetFileName(configPath).Replace(".json", ""), configPath, null), EMenuItemType.Submenu));
            
            foreach (var config in ConfigDao.ListDbConfigs())
                menuOptions.Add(new MenuItem("[DB]" + config.ConfigName,
                    () => ConfigActionsMenu(config.ConfigName, null, config), EMenuItemType.Submenu));
            
            menu.AddMenuItems(menuOptions);
            return menu.Run();
        }

        [SuppressMessage("ReSharper", "AccessToModifiedClosure")]
        private string ConfigActionsMenu(string? name, string? savePath, SavedConfig? config)
        {
            Menu menu = new Menu("Main Menu | Configs | Create / Modify", EMenuLevel.SecondOrMore);

            GameRules rules = default!;

            if (name == null)
                name = "default";
            
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("Name: ", () => (name), EMenuItemType.Info),
                new MenuItem("Board Size X: ", () => (rules.BoardSizeX.ToString()), EMenuItemType.Info),
                new MenuItem("Board Size Y: ", () => (rules.BoardSizeY.ToString()), EMenuItemType.Info),
                new MenuItem("Touchrule: ", () => (rules.EShipTouchRule.ToString()), EMenuItemType.Info),
                new MenuItem("Start Game", () => NewBsGameMenu(rules), EMenuItemType.Submenu),
                new MenuItem("Edit Name", () => { name = GetStringInput("Name", "[^a-zA-Z0-9]", 32); }, EMenuItemType.Submenu),
                new MenuItem("Edit X", () => rules.BoardSizeX = GetIntegerInput("X", 2, 26), EMenuItemType.Submenu),
                new MenuItem("Edit Y", () => rules.BoardSizeY = GetIntegerInput("Y", 2, 26), EMenuItemType.Submenu),
                new MenuItem("Edit Touchrule", () =>
                {
                    EShipTouchRule result;
                    Enum.TryParse(GetIntegerInput("0 - None, 1 - Corner, 2 - Side ", 0, 2).ToString(), true, out result);
                    rules.EShipTouchRule = result;
                }, EMenuItemType.Submenu),

                new MenuItem("Edit ships", () =>
                {
                    List<ShipConfig> newShipConfigs;
                    string result = ConfigShipsMenu(rules.ShipConfigs, out newShipConfigs);
                    rules.ShipConfigs = newShipConfigs;
                    return result;
                }, EMenuItemType.Submenu),
            });
            
            if (savePath != null) // local config
            {
                menu.AddMenuItem(new MenuItem("Save Config", () => { ConfigDao.SaveConfigLocal(name, rules); return " "; }, EMenuItemType.Submenu));
                menu.AddMenuItem(new MenuItem("Delete Config", () => { ConfigDao.RemoveConfigLocal(name); return " "; }, EMenuItemType.Submenu));
                rules = ConfigDao.GetConfigLocal(Path.GetFileName(savePath));
            }
            else if (config != null) // db config
            {
                menu.AddMenuItem(new MenuItem("Save Config", () =>
                {
                    ConfigDao.UpdateDbConfig(config.SavedConfigId!.Value, rules, name);
                    return " ";
                }, EMenuItemType.Submenu));
                
                menu.AddMenuItem(new MenuItem("Delete Config", () =>
                {
                    ConfigDao.RemoveConfigDb(config.SavedConfigId!.Value); 
                    return " ";
                }, EMenuItemType.Submenu));
                rules = JsonSerializer.Deserialize<GameRules>(config.ConfigJson)!;
            }
            else // new config
            {
                menu.AddMenuItem(new MenuItem("Save Config Locally", () => { ConfigDao.SaveConfigLocal(name, rules); return " "; }, EMenuItemType.Submenu));
                menu.AddMenuItem(new MenuItem("Save Config To DB", () =>
                {
                    ConfigDao.AddConfigDb(rules, name);
                    return " ";
                }, EMenuItemType.Submenu));
                rules = new GameRules();
            }
            return menu.Run();
        }
        
        private string ConfigShipsMenu(List<ShipConfig> shipsIn, out List<ShipConfig> shipsOut)
        {
            Menu menu = new Menu("Main Menu | Configs | Config Actions | Ships | Edit Ship", EMenuLevel.SecondOrMore);

            List<ShipConfig> ships = shipsIn;
            
            menu.AddMenuItem(new MenuItem("", () =>
            {
                menu.DeleteMenuMainItems();
                menu.AddMenuItem(new MenuItem("Create New Ship", () => ships.Add(new ShipConfig()
                {
                    Name = "New Ship",
                    Quantity = 1,
                    SizeX = 1,
                    SizeY = 1
                }), EMenuItemType.Submenu));
                
                foreach (var ship in ships)
                    menu.AddMenuItem(new MenuItem(ship.ToString(), () =>
                    {
                        ShipConfig shipOut;
                        var action = ShipActionsMenu(ship, out shipOut);
                        int i = ships.IndexOf(ship);
                        if (action == "save")
                        {
                            ships.Insert(i, shipOut);
                            ships.RemoveAt(i + 1);
                        } 
                        else if (action == "delete")
                        {
                            ships.RemoveAt(i);
                        }
                    }, EMenuItemType.Submenu));
            }, EMenuItemType.Task));
            
            string result = menu.Run();

            if (result == "save")
                shipsOut = ships;
            
            shipsOut = shipsIn;
            return result;
        }
        
        [SuppressMessage("ReSharper", "AccessToModifiedClosure")]
        private string ShipActionsMenu(ShipConfig? ship, out ShipConfig shipOut)
        {
            Menu menu = new Menu("Main Menu | Configs | Config Actions | Ships | Edit Ship", EMenuLevel.SecondOrMore);

            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("Name: ", () => ship!.Name!, EMenuItemType.Info),
                new MenuItem("X size: ", () => ship!.SizeX.ToString()!, EMenuItemType.Info),
                new MenuItem("Y size: ", () => ship!.SizeY.ToString()!, EMenuItemType.Info),
                new MenuItem("Quantity: ", () => ship!.Quantity.ToString()!, EMenuItemType.Info),
                new MenuItem("Edit Name", () => { ship!.Name = GetStringInput("New Name:", "[^a-zA-Z0-9]", 32); }, EMenuItemType.Submenu),
                new MenuItem("Edit X Size", () => { ship!.SizeX = GetIntegerInput("New Size X:",1, 10); }, EMenuItemType.Submenu),
                new MenuItem("Edit Y Size", () => { ship!.SizeY = GetIntegerInput("New Size Y:",1, 10); }, EMenuItemType.Submenu),
                new MenuItem("Edit Quantity", () => { ship!.Quantity = GetIntegerInput("New Quantity:",1, 10); }, EMenuItemType.Submenu),
                new MenuItem("Save Changes / Add Ship", () => "save", EMenuItemType.Submenu),
            });

            if (ship == null)
                ship = new ShipConfig()
                {
                    Name = "Patrol",
                    Quantity = 5,
                    SizeY = 1,
                    SizeX = 1
                };
            else
                menu.AddMenuItem(new MenuItem("Delete ship", () => "delete", EMenuItemType.Submenu));

            shipOut = ship;
            return menu.Run();
        }

        private string NewBsGameMenu(GameRules rules)
        {
            Menu menu = new Menu("New Game", EMenuLevel.SecondOrMore);

            BattleShipGame game = new BattleShipGame(rules);
            
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("Local Game", () => BsGameLocal(game), EMenuItemType.Submenu),
                new MenuItem("Multiplayer", () =>
                {
                    string name = GetStringInput("Enter a name for the game", "[^a-zA-Z0-9]", 32);
                    int gameId = GameDao.AddGameDb(game.CreateDto(), name);
                    return BsGameMultiPlayer(gameId);
                }, EMenuItemType.Submenu),
            });

            return menu.Run();
        }
        
        private string BSGamePlayerSetupMenu(GameRules rules, out Player playerOut, string playerName = "")
        {
            Menu menu = new Menu("Player Setup", EMenuLevel.Custom);
            if (playerName == "")
                playerName = GetStringInput("Enter your name", "[^a-zA-Z0-9]", 16);
            Player newPlayer = new Player(rules, playerName);
            
            menu.AddMenuItem(new MenuItem("", () =>
            {
                menu.DeleteMenuMainItems();
                menu.DeleteMenuInfoItems();

                menu.AddMenuItem(new MenuItem("", () =>
                {
                    foreach (var line in BoardToString(newPlayer.Board, false))
                        Console.WriteLine(line);
                }, EMenuItemType.Info));
                
                if (!newPlayer.SetupDone())
                    foreach (var ship in newPlayer.Ships)
                    {
                        if (!ship.IsPlaced())
                            menu.AddMenuItem(new MenuItem(ship.Name!, () =>
                            {
                                List<Coordinates> selection = GetBoardSelection(newPlayer.Board, ship.SizeX, ship.SizeY, rules.EShipTouchRule, false)!;
                                if (selection != null)
                                    newPlayer.PlaceShipOnBoard(selection, newPlayer.Ships.IndexOf(ship));
                            }, EMenuItemType.Submenu));
                    }
                else
                    menu.AddMenuItem(new MenuItem("Continue", () => { return " "; }, EMenuItemType.Submenu));
            }, EMenuItemType.Task));
            menu.AddMenuItem(new MenuItem("Quit To Main Menu", () => "root", EMenuItemType.Nav));
            playerOut = newPlayer;
                        
            return menu.Run();
        }
        
        private string BsGameLocal(BattleShipGame bsGame)
        {
            Menu menu = new Menu("Battleships", EMenuLevel.Custom);

            if (bsGame.ActiveGameState!.Player1 == null)
            {
                if (BSGamePlayerSetupMenu(bsGame.Rules, out bsGame.ActiveGameState.Player1) == "root") 
                    return "root";
                if (BSGamePlayerSetupMenu(bsGame.Rules, out bsGame.ActiveGameState.Player2) == "root") 
                    return "root";
                bsGame.PushGameState(null);
            }
            
            menu.AddMenuItems(new List<MenuItem>()
            {
                new MenuItem("", () => PrintState(bsGame.ActiveGameState, false), EMenuItemType.Info),
                new MenuItem("", () => bsGame.ActiveGameState.Describe(), EMenuItemType.Info),
                new MenuItem("Save Game", () => 
                    GameDao.SaveGameLocal(GetStringInput("Enter a name for the game", "[^a-zA-Z0-9]", 32), bsGame.CreateDto()),
                    EMenuItemType.Submenu),
                new MenuItem("Switch player", bsGame.SwitchPlayer, EMenuItemType.Submenu),
                new MenuItem("Bomb enemy", () =>
                    {
                        List<Coordinates> selection = GetBoardSelection(bsGame.GetEnemyPlayer()!.Board, 1, 1,
                            EShipTouchRule.Overlapping, true)!;
                        if (selection != null) 
                            bsGame.DetonateEnemySquare(selection[0]);
                    }, 
                    EMenuItemType.Submenu),
                new MenuItem("Quit To Main Menu", () => "root", EMenuItemType.Nav),
            });
            
            return menu.Run();
        }

        private string BsGameMultiPlayer(int gameId, string playerName = "")
        {
            Menu menu = new Menu("Battleships", EMenuLevel.Custom);
            
            if (playerName == "")
                playerName = GetStringInput("Enter your name", "[^a-zA-Z0-9]", 16);
            Game gameDbEntry = GameDao.GetDbGameById(gameId);
            BattleShipGame bsGame = new BattleShipGame(gameDbEntry);
            
            // setup
            if (bsGame.ActiveGameState!.Player1 == null)
            {
                if (BSGamePlayerSetupMenu(bsGame.Rules, out bsGame.ActiveGameState.Player1, playerName) == "root")
                {
                    // if game creator backs out of setup, delete the game altogether
                    GameDao.DeleteDbGameById(gameId);
                    return "root";
                }
                gameDbEntry.Player1 = playerName;
                bsGame.PushGameState(null);
                GameDao.PushGameStateDb(gameId, bsGame.ActiveGameState.CreateDto());
            } 
            // "join as second player" setup
            else if (bsGame.ActiveGameState!.Player2 == null && gameDbEntry.Player1 != playerName)
            {
                if (BSGamePlayerSetupMenu(bsGame.Rules, out bsGame.ActiveGameState.Player2, playerName) == "root") 
                    return "root";
                gameDbEntry.Player2 = playerName;
                bsGame.PushGameState(null);
                GameDao.PushGameStateDb(gameId, bsGame.ActiveGameState.CreateDto());
            }
            // main game loop
            menu.AddMenuItem(new MenuItem("", () =>
            {
                menu.DeleteMenuMainItems();
                menu.DeleteMenuInfoItems();

                try
                {
                    gameDbEntry = GameDao.GetDbGameById(gameId);
                }
                catch (Exception e)
                {
                    menu.AddMenuItem(new MenuItem($"{e.Message}", () => "", EMenuItemType.Info));
                    return;
                }
                bsGame = new BattleShipGame(gameDbEntry);


                if (bsGame.GetActivePlayer()!.Name == playerName && bsGame.ActiveGameState!.Player2 != null)
                    menu.AddMenuItems(new List<MenuItem>()
                    {
                        new MenuItem("", () => PrintState(bsGame.ActiveGameState, false), EMenuItemType.Info),
                        new MenuItem("", () => bsGame.ActiveGameState!.Describe(), EMenuItemType.Info),
                        new MenuItem("Bomb enemy", () =>
                            {
                                List<Coordinates> selection = GetBoardSelection(bsGame.GetEnemyPlayer()!.Board, 1, 1,
                                    EShipTouchRule.Overlapping, true)!;
                                if (selection != null) 
                                    bsGame.DetonateEnemySquare(selection[0]);
                                GameDao.PushGameStateDb(gameId, bsGame.ActiveGameState.CreateDto());
                            }, 
                            EMenuItemType.Submenu),
                    });
                else
                    menu.AddMenuItems(new List<MenuItem>()
                    {
                        new MenuItem("State: ", () => bsGame.ActiveGameState!.Describe(), EMenuItemType.Info),
                        new MenuItem("Refresh", () => "", EMenuItemType.Submenu),
                    });
            }, EMenuItemType.Task));
            menu.AddMenuItem(new MenuItem("Quit To Main Menu", () => "root", EMenuItemType.Nav));

            string result = menu.Run();
            return result;
        }
    }
}