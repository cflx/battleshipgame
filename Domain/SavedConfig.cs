﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    public class SavedConfig
    {
        public int? SavedConfigId { get; set; }

        [MaxLength(32)] 
        public string ConfigName { get; set; } = default!;

        [MaxLength(128000)] 
        public string ConfigJson { get; set; } = default!;
        public DateTime SavedAt { get; set; } = DateTime.Now;
    }
}