﻿using System;
using System.Collections.Generic;

namespace BattleShips
{
    public class GameDto
    {
        public GameRules? Rules { get; set; }
        public List<GameStateDto> GameStates { get; set; } = new List<GameStateDto>();
        public bool Finished { get; set; }
        
        public class GameStateDto
        {
            public PlayerDto? Player1 { get; set; }
            public PlayerDto? Player2 { get; set; }
            public bool ActivePlayer { get; set; }
            public DateTime DateTime { get; set; }
            public bool GameOver { get; set; }
            
            public class PlayerDto
            {
                public List<List<BoardSquare>>? Board { get; set; }
                public List<Ship>? Ships { get; set; }
                public string? Name { get; set; }
                public bool Winner { get; set; }
            }

        }
    }
}