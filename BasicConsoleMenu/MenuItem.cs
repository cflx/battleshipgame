﻿using System;

namespace BasicConsoleMenu
{
    public class MenuItem
    {
        public string Title { get; private set; }
        public Func<string>? RunMethod { get; private set; }
        public EMenuItemType MenuItemType { get; private set; }        
        
        public MenuItem(string title, Func<string>? runMethod, EMenuItemType itemType)
        {
            if (title == null)
                throw new ArgumentException("title is null!");
            
            Title = title;
            RunMethod = runMethod;
            MenuItemType = itemType;
        }
        
        public MenuItem(string title, Action runMethod, EMenuItemType itemType)
        {
            if (title == null)
                throw new ArgumentException("title is null!");
            
            Title = title.Trim();
            RunMethod = () =>
            {
                runMethod.Invoke();
                return "";
            };
            MenuItemType = itemType;
        }

        
        public override string ToString()
        {
            return Title;
        }

        public string GetInfo()
        {
            if (MenuItemType != EMenuItemType.Info)
                throw new Exception("Cannot call GetInfo on non INFO type object.");

            return Title + RunMethod!();
        }

        public void Invoke()
        {
            RunMethod!();
        }
    }
}