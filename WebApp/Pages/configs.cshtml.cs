﻿using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages;

public class configs : PageModel
{
    private readonly ConfigDao _configDao;

    public configs(ConfigDao configDao)
    {
        _configDao = configDao;
    }

    public DbSet<SavedConfig> SavedConfigs { get; set; } = default!;
    public List<string> LocalConfigs { get; set; } = default!;
    public IActionResult OnGet()
    {
        return NotFound();
    }
    
    public IActionResult OnPostAsync()
    {
        SavedConfigs = _configDao.ListDbConfigs();
        LocalConfigs = _configDao.ListLocalConfigs();

        return Page();
    }
}