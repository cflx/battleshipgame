﻿namespace BattleShips
{
    public class BoardSquare
    {
        public bool Boat { get; set; }
        public bool Detonated { get; set; }

        public BoardSquare() {}

        public BoardSquare(BoardSquare other)
        {
            Boat = other.Boat;
            Detonated = other.Detonated;
        }

        public override string ToString()
        {
            return (empty: Boat, Detonated) switch
            {
                (false, false) => " ",
                (false, true) => "*",
                (true, false) => "B",
                (true, true) => "@"
            };
        }
        
        public string ToStringAsEnemy()
        {
            return (empty: Boat, Detonated) switch
            {
                (false, false) => " ",
                (false, true) => "*",
                (true, false) => " ",
                (true, true) => "@"
            };
        }

    }
}