﻿using System.Collections.Generic;

namespace BattleShips
{
    public class Player
    {
        public List<List<BoardSquare>> Board { get; set; }
        public List<Ship> Ships { get; set; } = new List<Ship>();
        public string Name { get; set; }
        public bool Winner = false;
        
        public Player(GameRules rules, string name)
        {
            Board = InitializeBoard(rules.BoardSizeX, rules.BoardSizeY, null);
            Name = name;
            foreach (var shipConfig in rules.ShipConfigs)
                for (int count = 0; count < shipConfig.Quantity; count++)
                    Ships.Add(new Ship(shipConfig.Name, shipConfig.SizeX, shipConfig.SizeY));
        }

        public Player(Player other)
        {
            Board = InitializeBoard(other.Board.Count, other.Board[0].Count, other.Board);
            Name = other.Name;
            foreach (var ship in other.Ships)
                Ships.Add(new Ship()
                {
                    Name = ship.Name,
                    SizeX = ship.SizeX,
                    SizeY = ship.SizeY,
                    BSquares = ship.BSquares
                });
            Winner = other.Winner;
        }

        public Player(GameDto.GameStateDto.PlayerDto data)
        {
            Board = data.Board!;
            Ships = data.Ships!;
            Name = data.Name!;
            Winner = data.Winner;
        }
        
        public int ShipsIntact()
        {
            if (!SetupDone())
                return -1;
            int shipsIntact = Ships.Count;
            foreach (var ship in Ships)
            {
                int shipSquares = ship.BSquares.Count;
                foreach (var coord in ship.BSquares)
                    if (Board[coord.X][coord.Y].Detonated)
                        shipSquares--;
                if (shipSquares == 0)
                    shipsIntact--;
            }                
            return shipsIntact;
        }
        
        public bool SetupDone()
        {
            foreach (var ship in Ships)
                if (!ship.IsPlaced())
                    return false;
            return true;
        }
        
        public void PlaceShipOnBoard(List<Coordinates> coords, int shipIn)
        {
            Ship s = Ships[shipIn];
            
            foreach (var coord in coords)
            {
                s.BSquares = coords;
                Board[coord.X][coord.Y].Boat = true;
            }
        }
        
        private List<List<BoardSquare>> InitializeBoard(int bWidth, int bHeight, List<List<BoardSquare>>? other)
        {
            List<List<BoardSquare>> board = new List<List<BoardSquare>>();

            for (var x = 0; x < bWidth; x++)
            {
                board.Add(new List<BoardSquare>());
                for (var y = 0; y < bHeight; y++)
                    if (other == null)
                        board[x].Add(new BoardSquare
                        {
                            Boat = false,
                            Detonated = false
                        });
                    else
                        board[x].Add(new BoardSquare
                        {
                            Boat = other[x][y].Boat,
                            Detonated = other[x][y].Detonated
                        });
            }
            return board;
        }
        
        public void DetonateSquare(Coordinates coord)
        {
            Board[coord.X][coord.Y].Detonated = true;
        }

        public GameDto.GameStateDto.PlayerDto CreateDto()
        {
            return new GameDto.GameStateDto.PlayerDto()
            {
                Board = Board,
                Name = Name,
                Ships = Ships,
                Winner = Winner
            };
        }
    }
}