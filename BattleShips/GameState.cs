﻿using System;

namespace BattleShips;

public class GameState
{
    public Player? Player1;
    public Player? Player2;
    public bool GameOver = false;
    public DateTime DateTime = DateTime.Now;

    // false for p1, true for p2
    public bool Activeplayer = false;

    public GameState() {}

    public GameState(GameState state)
    {
        Player1 = state.Player1 != null ? new Player(state.Player1) : null;
        Player2 = state.Player2 != null ? new Player(state.Player2) : null;
        GameOver = state.GameOver;
        Activeplayer = state.Activeplayer;
    }

    public GameState(GameDto.GameStateDto data)
    {
        if (data.Player1 != null) 
            Player1 = new Player(data.Player1);
        if (data.Player2 != null) 
            Player2 = new Player(data.Player2);
        GameOver = data.GameOver;
        Activeplayer = data.ActivePlayer;
        DateTime = data.DateTime;
    }

    public string Describe()
    {
        if (Player1 == null || Player2 == null)
            return "Waiting for opponent...";
        string state = $"{Player1!.Name} - {Player1.ShipsIntact()} ships | " +
                       $"{Player2!.Name} - {Player2.ShipsIntact()} ships | ";
        if (GameOver)
            return state + (Player1!.Winner ? Player1!.Name : Player2!.Name) + " has won!";
        else
            return state + (Activeplayer ? Player2!.Name : Player1!.Name) + "\'s turn!";
    }
    
    public GameDto.GameStateDto CreateDto()
    {
        return new GameDto.GameStateDto()
        {
            Player1 = Player1 != null ? Player1!.CreateDto() : null,
            Player2 = Player2 != null ? Player2!.CreateDto() : null,
            ActivePlayer = Activeplayer,
            DateTime = DateTime,
            GameOver = GameOver
        };
    }
}