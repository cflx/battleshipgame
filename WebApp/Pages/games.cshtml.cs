﻿using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages;

public class games : PageModel
{
    private readonly GameDao _gameDao;

    public games(GameDao gameDao)
    {
        _gameDao = gameDao;
    }

    public string? UserName { get; set; }
    public List<Game> DbGames { get; set; } = default!;
    public string[] LocalGames { get; set; } = default!;

    public IActionResult OnPostAsync()
    {
        UserName = Request.Cookies["UserName"];
        if (UserName != null)
            DbGames = _gameDao.ListDbGamesWithPlayer(UserName).ToList();

        LocalGames = _gameDao.ListLocalGames();

        return Page();
    }
}