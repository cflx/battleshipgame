﻿using System.Resources;
using System.Text.Json;
using BattleShips;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using GameState = Domain.GameState;

namespace WebApp.Pages;

public class game : PageModel
{
    private readonly GameDao _gameDao;
    
    public game(GameDao gameDao)
    {
        _gameDao = gameDao;
    }
    
    public string? UserName { get; set; }
    public BattleShipGame? Game { get; set; }
    public string? GameType { get; set;}
    
    [BindProperty(SupportsGet = true)]
    public int? Id { get; set; }
    [BindProperty(SupportsGet = true)]
    public string? Fname { get; set; }
    [BindProperty(SupportsGet = true)]
    public int? Delta { get; set; } // how many states back from the latest gamestate

    private string GetGame()
    {
        UserName = Request.Cookies["UserName"];
        if (UserName == null) return "Username not set! (Unauthorized access)";
        
        if (Id != null)
        {
            Game? dbGame;
            try
            {
                dbGame = _gameDao.GetDbGameById(Id.Value);
            }
            catch (Exception e)
            {
                return e.Message;
            }

            if (dbGame.Player1 != UserName && dbGame.Player2 != UserName && dbGame.Player2 != null)
                return $"User with {UserName} is not in the game! (Unauthorized access)";
            Game = new BattleShipGame(dbGame);
            if (Delta > 0)
            {
                GameDto.GameStateDto state = JsonSerializer.Deserialize<GameDto.GameStateDto>(
                        dbGame.GameStates!.ToList()[dbGame.GameStates!.Count - Delta.Value].GameStateJson)!;
                Game.ActiveGameState = new BattleShips.GameState(state);
            }
            GameType = "multi";
            return "";
        }
        else if (Fname != null)
        {
            GameDto? gameDto;
            try
            {
                gameDto = _gameDao.LoadGameLocal(Fname);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            Game = new BattleShipGame(gameDto);
            if (Delta > 0)
            {
                int index = Game.GameStates.Count - Delta.Value;
                if (index < 0)
                    return "No earlier gamestates.";
                Game.PushGameState(Game.GameStates[^Delta.Value]);
            }
            GameType = "local";
            return "";
        }
        return "Game ID or filename not specified!";
    }

    private string SaveGame()
    {
        if (Id != null)
        {
            try
            {
                _gameDao.PushGameStateDb(Id.Value, Game!.ActiveGameState!.CreateDto());
            }
            catch (Exception)
            {
                return $"Failed to update state of game with id {Id}";
            }
            return "";
        }
        else if (Fname != null)
        {
            _gameDao.SaveGameLocal(Fname, Game!.CreateDto());
            return "";
        }
        return "Game ID or filename not specified!";
    }
    
    public IActionResult OnGet()
    {
        string result = GetGame();
        if (result != "")
        {
            return BadRequest(result);
        }

        if (Id != null && Game!.ActiveGameState!.Player1!.Name != UserName && Game!.ActiveGameState!.Player2 == null)
            return RedirectToPage("newgame", new {type = "join", gameId = Id});
        
        if (Delta > 0 && !Game!.Finished)
            return RedirectToPage("game", (Id != null ? new {id = Id} : new {fname = Fname}));
        
        return Page();
    }
    
    public IActionResult OnPost(string? cmd, string? coords)
    {
        string result = GetGame();
        if (result != "") return BadRequest(result);
    
        if (cmd == "bomb")
        {
            if (Fname != null || Id != null && Game!.GetActivePlayer()!.Name == UserName)
                if (coords != null)
                    Game!.DetonateEnemySquare(JsonSerializer.Deserialize<Coordinates>(coords)!);
                else
                    return BadRequest("Coordinates not specified!");
            else
                return BadRequest("Not your turn!");
        } // only local games
        else if (Id == null && cmd == "switch")
            Game!.SwitchPlayer();
        
        Game!.PushGameState(null);
        result = SaveGame();
        
        if (result != "") return BadRequest(result);

        return RedirectToPage("game", (Id != null ? new {id = Id} : new {fname = Fname}));
    }
    
    public IActionResult OnPostDelete()
    {
        string result = GetGame();
        if (result != "") return BadRequest(result);

        if (Id != null)
            _gameDao.DeleteDbGameById(Id.Value);
        else if (Fname != null)
            _gameDao.DeleteLocalByName(Fname);
        
        return StatusCode(200);
    }

    public IActionResult OnPostJoin(string player)
    {
        string result = GetGame();
        if (result != "") return BadRequest(result);

        return NotFound();
    }

}