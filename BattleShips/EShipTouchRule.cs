﻿namespace BattleShips
{
    public enum EShipTouchRule
    {
        None,
        Corner,
        Side,
        Overlapping
    }
}