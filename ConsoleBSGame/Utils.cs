﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using BattleShips;

namespace ConsoleBSGame
{
    static class Utils
    {
        public static bool GetBoolInput(string label)
        {
            return GetIntegerInput(label, 0, 1) == 1;
        }
        
        public static int GetIntegerInput(string label, int min, int max)
        {
            var done = false;
            var error = false;
            int value = 0;
            
            while (!done)
            {
                Console.Clear();
                if (error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid input!");
                    Console.ResetColor();
                }

                Console.Write(label + "(" + min + "-" + max +"): ");

                try
                {
                    value = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    error = true;
                    continue;
                }

                if (value < min || value > max)
                    error = true;
                else
                    done = true;
            }
            return value;
        }

        public static string GetStringInput(string prompt, string pattern, int maxLen)
        {
            string str = "";
            bool error = false;

            while (str == "")
            {
                Console.Clear();

                if (error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Input cannot be empty/whitespaces, contain symbols or be longer than " + maxLen
                                        + " characters!");
                    Console.ResetColor();
                }

                Console.Write(prompt + ": ");

                str = Console.ReadLine()!;
                str = Regex.Replace(str, pattern, string.Empty);
                str = str.Trim();
                if (str.Length > maxLen)
                    str = "";
                error = true;
            }
            return str;
        }
        
        public static void PrintState(GameState state, bool postGame)
        {
            int spacing = 5;            
            
            string[] curPlayerBoard = !state.Activeplayer ? 
                BoardToString(state.Player1!.Board, false): BoardToString(state.Player2!.Board, false);
            string[] enemyPlayerBoard = !state.Activeplayer ?
                BoardToString(state.Player2!.Board, !postGame) : BoardToString(state.Player1!.Board, !postGame);

            int pos = Console.GetCursorPosition().Top;
            int secondCol = curPlayerBoard[0].Length + spacing;
            
            Console.Write(!state.Activeplayer ? 
                state.Player1!.Name : state.Player2!.Name);
            Console.SetCursorPosition(secondCol, pos);
            Console.Write(state.Activeplayer ? 
                state.Player1!.Name : state.Player2!.Name);

            for (int i = 0; i < curPlayerBoard.Length; i++)
            {
                Console.SetCursorPosition(0, ++pos);
                Console.Write(curPlayerBoard[i]);
                Console.SetCursorPosition(secondCol, pos);
                Console.Write(enemyPlayerBoard[i]);
            }
        }

        public static string[] BoardToString(List<List<BoardSquare>> board, 
                                             bool enemyPov, 
                                             List<Coordinates>? selected = null)
        {
            StringBuilder asStr = new StringBuilder();

            asStr.Append(" ||");
            for (var x = 0; x < board.Count; x++)
                asStr.Append($" { Convert.ToChar(65 + x) } |");

            asStr.Append("\n");
            
            string spacer = new string('—', board.Count * 4 + 3);

            for (var y = 0; y < board[0].Count; y++)
            {
                if (y == 0)
                    asStr.Append(spacer + "\n");

                StringBuilder line = new StringBuilder(String.Format("{0, 2}|", y + 1));
                for (var x = 0; x < board.Count; x++)
                    if (selected != null && selected.Contains(new Coordinates(x, y)))
                        line.Append(" ● |");
                    else if (enemyPov)
                        line.Append($" {board[x][y].ToStringAsEnemy()} |");
                    else
                        line.Append($" {board[x][y]} |");

                asStr.Append(line + ((y != board[0].Count - 1) ? "\n" : ""));
            }
            return asStr.ToString().Split("\n");
        }
        public static List<Coordinates>? GetBoardSelection(List<List<BoardSquare>> board, 
                                                           int selectX, 
                                                           int selectY, 
                                                           EShipTouchRule rule, 
                                                           bool enemyPov)
        {
            List<Coordinates> selection = new List<Coordinates>();

            for (int y = 0; y < selectY; y++)
                for (int x = 0; x < selectX; x++)
                    selection.Add(new Coordinates(x, y));
            
            bool success = false;
            while (!success)
            {
                Console.Clear();
                foreach (var line in BoardToString(board, enemyPov, selection))
                    Console.WriteLine(line);
                Console.WriteLine($"Touchrule: { rule.ToString() }\n");
                Console.WriteLine("Controls:\nArrow keys - Movement\nR - Rotate\nEnter - Submit\nBackspace - Return");

                List<Coordinates> newSelection = new List<Coordinates>();
                ConsoleKey key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.UpArrow)
                    selection.ForEach(c => newSelection.Add(new Coordinates(c.X, c.Y - 1)));
                else if (key == ConsoleKey.LeftArrow)
                    selection.ForEach(c => newSelection.Add(new Coordinates(c.X - 1, c.Y)));
                else if (key == ConsoleKey.DownArrow)
                    selection.ForEach(c => newSelection.Add(new Coordinates(c.X, c.Y + 1)));
                else if (key == ConsoleKey.RightArrow)
                    selection.ForEach(c => newSelection.Add(new Coordinates(c.X + 1, c.Y)));
                else if (key == ConsoleKey.R)
                    selection.ForEach(c => newSelection.Add(new Coordinates(c.Y, c.X)));
                else if (key == ConsoleKey.Backspace) return null;

                // cant go OOB
                if (newSelection.Count > 0 && !newSelection.Exists(c => c.X < 0 || c.Y < 0 || c.X == board.Count || c.Y == board[0].Count))
                    selection = newSelection;

                if (key == ConsoleKey.Enter && BattleShipGame.IsPlacementValid(board, rule, selection))
                    success = true;
            }

            return selection;
        }
        
    }
}