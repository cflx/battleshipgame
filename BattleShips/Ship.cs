﻿using System.Collections.Generic;

namespace BattleShips
{
    public class Ship
    {
        public string? Name { get; set; }
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public List<Coordinates> BSquares { get; set; } = new List<Coordinates>();

        public Ship() {}

        public Ship(string? name, int? sizeX, int? sizeY)
        {
            Name = name;
            SizeX = (sizeX != null) ? sizeX.Value : 1;
            SizeY = (sizeY != null) ? sizeY.Value : 1;
        }

        public bool IsPlaced()
        {
            return BSquares.Count == (SizeX * SizeY);
        }
    }
}