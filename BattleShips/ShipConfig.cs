﻿namespace BattleShips
{
    public class ShipConfig
    {
        public string? Name { get; set; }
        public int? Quantity { get; set; }
        public int? SizeY { get; set; } 
        public int? SizeX { get; set; }

        public override string ToString()
        {
            return Quantity + "x of " + Name + "(" + SizeX + "x" + SizeY + ")";
        }
    }
}