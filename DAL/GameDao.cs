﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BattleShips;
using Domain;
using Microsoft.EntityFrameworkCore;
using GameState = Domain.GameState;

namespace DAL
{
    public class GameDao
    {
        private static string BasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/Battleships";
        
        private string SavesPath =>
            BasePath + Path.DirectorySeparatorChar
                     + "Saves" + Path.DirectorySeparatorChar;
        
        private AppDbContext DbContext = new();

        public int AddGameDb(GameDto game, string name)
        {
            Game newGame = new Game()
            {
                GameName = name,
                Player1 = game.GameStates.Count != 0 ? game.GameStates[game.GameStates.Count - 1].Player1?.Name : null,
                Player2 = game.GameStates.Count != 0 ? game.GameStates[game.GameStates.Count - 1].Player2?.Name : null,
                Finished = game.Finished,
                GameRuleJson = JsonSerializer.Serialize(game.Rules),
                GameStates = new List<GameState>()
            };
            
            foreach (var stateDto in game.GameStates)
            {
                newGame.GameStates!.Add(new GameState()
                {
                    GameStateJson = JsonSerializer.Serialize(stateDto),
                    CreatedAt = stateDto.DateTime
                });
            }

            DbContext.Games.Add(newGame);
            DbContext.SaveChanges();

            return newGame.GameId;
        }

        public void PushGameStateDb(int gameId, GameDto.GameStateDto gamestate)
        {
            Game game = GetDbGameById(gameId);

            // set game as finished if ever a gamestate is pushed that has gameover
            if (gamestate.GameOver)
                game.Finished = true;
            
            game.GameStates!.Add(new GameState()
            {
                GameStateJson = JsonSerializer.Serialize(gamestate),
                CreatedAt = DateTime.Now
            });
            
            DbContext.SaveChanges();
        }

        public IEnumerable<Game> ListDbGamesWithPlayer(string name)
        {
            return DbContext.Games.Where(game => game.Player1 == name || game.Player2 == name || game.Player2 == null);
        }

        public Game GetDbGameById(int gameId)
        {
            Game? game = DbContext.Games
                .Include(g => g.GameStates)
                .FirstOrDefault(g => g.GameId == gameId);
            if (game != null)
                return game;
            
            throw new Exception($"ERROR: Game with ID {gameId} doesn't exist in db!");
        }

        public void DeleteDbGameById(int gameId)
        {
            DbContext.Games.Remove(GetDbGameById(gameId));
            DbContext.SaveChanges();
        }
        
        public string[] ListLocalGames()
        {
            if (!Directory.Exists(SavesPath))
                Directory.CreateDirectory(SavesPath);
            
            string[] files = Directory.GetFiles(SavesPath);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]).Replace(".json", "");

            return files;
        }
        
        public GameDto LoadGameLocal(string name)
        {
            GameDto? game = JsonSerializer.Deserialize<GameDto>(File.ReadAllText(SavesPath + AddJsonFileExtension(name)));
            if (game != null)
                return game;
            
            throw new Exception($"ERROR: Local game with name {name} doesn't exist!");
        }
        
        public void SaveGameLocal(string name, GameDto game)
        {
            
            string gameJson = JsonSerializer.Serialize(game, new JsonSerializerOptions() { WriteIndented = false });
            File.WriteAllText(SavesPath + AddJsonFileExtension(name), gameJson);
        }

        public void DeleteLocalByName(string name)
        {
            if(File.Exists(SavesPath + AddJsonFileExtension(name)))
                File.Delete(SavesPath + AddJsonFileExtension(name));
        }

        private string AddJsonFileExtension(string name)
        {
            return !name.Contains(".json") ? name + ".json" : name;
        }
    }
}
