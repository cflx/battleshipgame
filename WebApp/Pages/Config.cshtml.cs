﻿using System.Text.Json;
using BattleShips;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages;

public class Config : PageModel
{
    private readonly ConfigDao _configDao;

    public Config(ConfigDao configDao)
    {
        _configDao = configDao;
    }

    public GameRules Conf { get; set; } = new GameRules();
    public string Name { get; set; } = "default";
    public int? Id { get; set; }

    // used when generating the page to be able to assign "null" to submitbutton onclick function parameters
    // without this an uninitialized nullable "prints" nothing to the page and then the js dont work :(
    public string GetIdOrNull()
    {
        if (Id != null)
            return Id.ToString()!;

        return "null";
    }
    
    public async Task<IActionResult> OnGetAsync(int? id, string? fname)
    {
        if (id != null)
        {
            SavedConfig dbConf;
            try
            {
                dbConf = await _configDao.GetDbConfigByIdAsync(id.Value);
            }
            catch (Exception)
            {
                return Page();
            }
            Id = id;
            Name = dbConf.ConfigName;
            Conf = JsonSerializer.Deserialize<GameRules>(dbConf.ConfigJson)!;
        }
        else if (fname != null)
        {
            Name = fname;
            try
            {
                GameRules conf = _configDao.GetConfigLocal(fname);
                Conf = conf;
            }
            catch (Exception) {// ignored, fall back to default config
            }

        }
        
        return Page();
    }

    public IActionResult OnPost(int? id, string? cmd, string? name, string? configJson)
    {
        if (configJson != null && name != null)
        {
            GameRules rules;
            try
            {
                rules = JsonSerializer.Deserialize<GameRules>(configJson)!;
            }
            catch (Exception)
            {
                return StatusCode(400);
            }

            if (cmd == "saveLocal")
            {
                _configDao.SaveConfigLocal(name, rules);
                return StatusCode(200);
            }
            else if (cmd == "saveDb")
            {
                if (id != null)
                    _configDao.UpdateDbConfig(id.Value, rules, name);
                else
                    _configDao.AddConfigDb(rules, name);
                
                return StatusCode(200);
            }
        } else if (cmd == "delete")
        {
            if (id != null)
            {
                _configDao.RemoveConfigDb(id.Value);
                return StatusCode(200);
            } 
            else if (name != null)
            {
                _configDao.RemoveConfigLocal(name);
                return StatusCode(200);
            }
        }

        return StatusCode(400);
    }
}