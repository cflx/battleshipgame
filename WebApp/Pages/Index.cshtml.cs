﻿using System.ComponentModel.DataAnnotations;
using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages;

public class IndexModel : PageModel
{
    public string? UserName { get; set; }
    
    public IActionResult OnGet()
    {
        UserName = Request.Cookies["UserName"];
        return Page();
    }

    public IActionResult OnPost(string? userName)
    {
        // ghetto serverside validation just in case???
        if (userName != null && userName.Length > 2 && userName.Length <= 16)
        {
            CookieOptions option = new CookieOptions();  
            Response.Cookies.Append("UserName", userName, option);
        }

        return Redirect("/");
    }
    
}