﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Domain
{
    public class Game
    {
        public int GameId { get; set; }
        
        [MaxLength(32)]
        public string? GameName { get; set; }
        [MaxLength(16)]
        public string? Player1 { get; set; }
        [MaxLength(16)]
        public string? Player2 { get; set; }
        public bool Finished { get; set; }
        public string? GameRuleJson { get; set; }
        public ICollection<GameState>? GameStates { get; set; }

        public GameState GetLatestGameState()
        {
            return GameStates!.OrderByDescending(x => x.CreatedAt).First();
        }
    }
}
