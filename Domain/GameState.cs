﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    [Index(nameof(CreatedAt))]
    public class GameState
    {
        public int GameStateId { get; set; }
        [Required]
        public Game? Game { get; set; }

        [MaxLength(128000)] 
        public string GameStateJson { get; set; } = default!; 
        public DateTime CreatedAt { get; set; }
    }
}