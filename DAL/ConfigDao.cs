﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using BattleShips;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class ConfigDao
    {
        private static string BasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/Battleships";
        
        private static string ConfigPath =>
            BasePath + Path.DirectorySeparatorChar
                     + "Configs" + Path.DirectorySeparatorChar;

        private AppDbContext DbContext = new();

        public List<string> ListLocalConfigs()
        {
            if (!Directory.Exists(ConfigPath))
                Directory.CreateDirectory(ConfigPath);

            string[] files = Directory.GetFiles(ConfigPath);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]).Replace(".json", "");

            return files.ToList();
        }

        public void SaveConfigLocal(string name, GameRules conf)
        {
            if (!Directory.Exists(ConfigPath))
                Directory.CreateDirectory(ConfigPath);
            
            string configJson = JsonSerializer.Serialize(conf, new JsonSerializerOptions() { WriteIndented = true });
            File.WriteAllText(ConfigPath + name + ".json", configJson);
        }

        public void RemoveConfigLocal(string name)
        {
            name = AddJsonFileExtension(name);
            if(File.Exists(ConfigPath + name))
                File.Delete(ConfigPath + name);
        }
        
        public GameRules GetConfigLocal(string name)
        {
            string fileName;
            if (!name.Contains(".json"))
                fileName = name + ".json";
            else
                fileName = name;
            
            if (File.Exists(ConfigPath + fileName))
            {
                string config = File.ReadAllText(ConfigPath + fileName);
                return JsonSerializer.Deserialize<GameRules>(config)!;
            }
            else if (fileName == "default.json")
            {
                GameRules rules = new GameRules();
                string confJsonStr = JsonSerializer.Serialize(rules, new JsonSerializerOptions() { WriteIndented = true });

                if (!Directory.Exists(ConfigPath))
                    Directory.CreateDirectory(ConfigPath);

                File.WriteAllText(ConfigPath + fileName, confJsonStr);

                return rules;
            }

            throw new Exception("Local config " + name + " doesn't exist!");
        }

        public SavedConfig GetDbConfigById(int configId)
        {
            SavedConfig? config = DbContext.SavedConfigs
                .FirstOrDefault(g => g.SavedConfigId == configId);
            if (config != null)
                return config;
            else
                throw new Exception($"ERROR: Config with ID {configId} doesn't exist in db!");
        }
        
        public async Task<SavedConfig> GetDbConfigByIdAsync(int configId)
        {
            SavedConfig? config = await DbContext.SavedConfigs
                .FirstOrDefaultAsync(g => g.SavedConfigId == configId);
            if (config != null)
                return config;
            
            throw new Exception($"ERROR: Config with ID {configId} doesn't exist in db!");
        }
        
        public void UpdateDbConfig(int id, GameRules newRules, string newName)
        {
            SavedConfig conf = GetDbConfigById(id);
            
            conf.ConfigJson = JsonSerializer.Serialize(newRules);
            conf.ConfigName = newName;
            conf.SavedAt = DateTime.Now;
            
            DbContext.SavedConfigs.Update(conf);
            DbContext.SaveChanges();
        }

        public void AddConfigDb(GameRules conf, string name)
        {
            DbContext.SavedConfigs.Add(new SavedConfig()
            {
                ConfigName = name,
                ConfigJson = JsonSerializer.Serialize(conf)
            });
            
            DbContext.SaveChanges();
        }

        public void RemoveConfigDb(int id)
        {
            DbContext.SavedConfigs.Remove(GetDbConfigById(id));
            DbContext.SaveChanges();
        }
        
        public DbSet<SavedConfig> ListDbConfigs()
        {
            return DbContext.SavedConfigs;
        }
        
        private string AddJsonFileExtension(string name)
        {
            return !name.Contains(".json") ? name + ".json" : name;
        }
    }
}