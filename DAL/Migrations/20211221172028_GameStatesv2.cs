﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    public partial class GameStatesv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameState_SavedGames_SavedGameId",
                table: "GameState");

            migrationBuilder.DropTable(
                name: "SavedGames");

            migrationBuilder.DropIndex(
                name: "IX_GameState_SavedGameId",
                table: "GameState");

            migrationBuilder.DropColumn(
                name: "SavedGameId",
                table: "GameState");

            migrationBuilder.RenameColumn(
                name: "SavedAt",
                table: "GameState",
                newName: "CreatedAt");

            migrationBuilder.RenameIndex(
                name: "IX_GameState_SavedAt",
                table: "GameState",
                newName: "IX_GameState_CreatedAt");

            migrationBuilder.AlterColumn<string>(
                name: "ConfigName",
                table: "SavedConfigs",
                type: "varchar(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldMaxLength: 255,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<int>(
                name: "GameId",
                table: "GameState",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GameName = table.Column<string>(type: "varchar(32)", maxLength: 32, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Player1 = table.Column<string>(type: "varchar(16)", maxLength: 16, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Player2 = table.Column<string>(type: "varchar(16)", maxLength: 16, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Finished = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    GameRuleJson = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.GameId);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_GameState_GameId",
                table: "GameState",
                column: "GameId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameState_Games_GameId",
                table: "GameState",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameState_Games_GameId",
                table: "GameState");

            migrationBuilder.DropTable(
                name: "Games");

            migrationBuilder.DropIndex(
                name: "IX_GameState_GameId",
                table: "GameState");

            migrationBuilder.DropColumn(
                name: "GameId",
                table: "GameState");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "GameState",
                newName: "SavedAt");

            migrationBuilder.RenameIndex(
                name: "IX_GameState_CreatedAt",
                table: "GameState",
                newName: "IX_GameState_SavedAt");

            migrationBuilder.AlterColumn<string>(
                name: "ConfigName",
                table: "SavedConfigs",
                type: "varchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(32)",
                oldMaxLength: 32,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<int>(
                name: "SavedGameId",
                table: "GameState",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SavedGames",
                columns: table => new
                {
                    SavedGameId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GameName = table.Column<string>(type: "varchar(32)", maxLength: 32, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Player1 = table.Column<string>(type: "varchar(16)", maxLength: 16, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Player2 = table.Column<string>(type: "varchar(16)", maxLength: 16, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedGames", x => x.SavedGameId);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_GameState_SavedGameId",
                table: "GameState",
                column: "SavedGameId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameState_SavedGames_SavedGameId",
                table: "GameState",
                column: "SavedGameId",
                principalTable: "SavedGames",
                principalColumn: "SavedGameId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
