﻿using System.Text.Json;
using BattleShips;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using GameState = BattleShips.GameState;

namespace WebApp.Pages;

public class newgame : PageModel
{
    private readonly ConfigDao _configDao;    
    private readonly GameDao _gameDao;    

    public newgame(ConfigDao configDao, GameDao gameDao)
    {
        _gameDao = gameDao;
        _configDao = configDao;
    }

    public string GameType { get; set; } = "local";
    public GameRules Config { get; set; } = new GameRules();
    public string ConfigName { get; set; } = "default";
    public int ConfigId { get; set; } = -1;
    public string? UserName { get; set; }
    public int GameId { get; set; } = -1;

    public async Task<IActionResult> OnGetAsync(string? type, int? id, int? gameId, string? fname)
    {
        UserName = Request.Cookies["UserName"];

        if (type == "local" || type == "multi" || type == "join")
            GameType = type;
        else if (type != null)
            return BadRequest("invalid gametype!");
        
        if (id != null)
        {
            SavedConfig dbConf;
            try
            {
                dbConf = await _configDao.GetDbConfigByIdAsync(id.Value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            ConfigId = id.Value;
            Config = JsonSerializer.Deserialize<GameRules>(dbConf.ConfigJson)!;
        }   
        else if (fname != null)
        {
            Config = _configDao.GetConfigLocal(fname);
            ConfigName = fname;
        } 
        else if (gameId != null)
        {
            GameId = gameId.Value;
            BattleShipGame game = new BattleShipGame(_gameDao.GetDbGameById(gameId.Value));
            Config = game.Rules;
        }
        
        return Page();
    }
    
    public string ValidatePlayer(Player p)
    {
        if ((p.ShipsIntact() != Config.GetTotalShipCount()))
            return "All ships in config are not (entirely?) present player" + p.Name + "'s data!";

        Player pValidate = new Player(Config, " ");
        foreach (var ship in p.Ships)
        {
            bool result = BattleShipGame.IsPlacementValid(pValidate.Board, Config.EShipTouchRule, ship.BSquares);
            if (!result)
                return "A ship does not adhere to touchrules in " + p.Name + "'s data!";
            else
                foreach (var coord in ship.BSquares)
                    pValidate.Board[coord.X][coord.Y].Boat = true;
        }

        return "";
    }
    
    public IActionResult OnPost(string? name, string? type, int? configId, int? gameId, string? fname, string? p1, string? p2)
    {
        if (!(type == "local" || type == "multi" || type == "join")) return BadRequest("Invalid gametype!");
        if (configId == null && fname == null) return BadRequest("Config not specified!");
        if (name == null && type != "join") return BadRequest("Game name not specified!");
        
        Player player1 = default!;
        Player player2 = default!;
        if (p1 != null)
        {
            try
            {
                player1 = new Player(JsonSerializer.Deserialize<GameDto.GameStateDto.PlayerDto>(p1)!);
                if (type == "local")
                {
                    player2 = new Player(JsonSerializer.Deserialize<GameDto.GameStateDto.PlayerDto>(p2!)!);
                }
            }
            catch (Exception)
            {
                return BadRequest("Error deserializing player(s)!");
            }
        }

        GameRules? conf = null;
        if (configId != null)
        {
            try
            {
                string json = _configDao.GetDbConfigById(configId.Value).ConfigJson;
                conf = JsonSerializer.Deserialize<GameRules>(json);
            }
            catch (Exception){ // ignored
            }
        } 
        else if (fname != null && conf == null)
        {
            try
            {
                conf = _configDao.GetConfigLocal(fname);
            }
            catch (Exception){ // ignored
            }
        }
        else if (conf == null && type != "join")
            return BadRequest("Config not specified / doesn't exist!");

        if (gameId != null)
        {
            GameId = gameId.Value;
            BattleShipGame game = new BattleShipGame(_gameDao.GetDbGameById(gameId.Value));
            Config = game.Rules;
        }
        else
            Config = conf!;
       
        
        string result;
        result = ValidatePlayer(player1);
        if (result != "")
            return BadRequest(result);
        // this shit is a travesty
        if (type == "local")
        {
            result = ValidatePlayer(player2);
            if (result != "")
                return BadRequest(result);
        
            GameState newState = new BattleShips.GameState() {Player1 = player1, Player2 = player2};
            BattleShipGame game = new BattleShipGame(conf!);
            game.GameStates = new List<GameState>() { newState };
            game.ActiveGameState = newState;
            game.PushGameState(null);

            _gameDao.SaveGameLocal(name, game.CreateDto());
            return StatusCode(200);
        }
        else if (type == "multi")
        {
            GameState newState = new BattleShips.GameState() {Player1 = player1, Player2 = player2};
            BattleShipGame game = new BattleShipGame(conf!);
            game.GameStates = new List<GameState>() { newState };
            game.ActiveGameState = newState;
            game.PushGameState(null);

            _gameDao.AddGameDb(game.CreateDto(), name);
            return StatusCode(200);
        } 
        else if (type == "join")
        { 
            Game dbGame = _gameDao.GetDbGameById(gameId!.Value);
            BattleShipGame game = new BattleShipGame(dbGame);
            game.ActiveGameState!.Player2 = player1;
            dbGame.Player2 = player1.Name;
            _gameDao.PushGameStateDb(gameId.Value, game.ActiveGameState.CreateDto());

            return StatusCode(200);
        }

        return BadRequest("Unknown error");
    }
}