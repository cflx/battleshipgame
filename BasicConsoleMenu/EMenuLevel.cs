namespace BasicConsoleMenu
{
    public enum EMenuLevel
    {
        Root,
        First,
        SecondOrMore,
        Custom
    }
}