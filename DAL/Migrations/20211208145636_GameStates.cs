﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    public partial class GameStates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GameJson",
                table: "SavedGames");

            migrationBuilder.DropColumn(
                name: "SavedAt",
                table: "SavedGames");

            migrationBuilder.RenameColumn(
                name: "GameId",
                table: "SavedGames",
                newName: "SavedGameId");

            migrationBuilder.RenameColumn(
                name: "ConfigId",
                table: "SavedConfigs",
                newName: "SavedConfigId");

            migrationBuilder.AlterColumn<string>(
                name: "GameName",
                table: "SavedGames",
                type: "varchar(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldMaxLength: 128000,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Player1",
                table: "SavedGames",
                type: "varchar(16)",
                maxLength: 16,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Player2",
                table: "SavedGames",
                type: "varchar(16)",
                maxLength: 16,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "SavedAt",
                table: "SavedConfigs",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldRowVersion: true,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "GameState",
                columns: table => new
                {
                    GameStateId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SavedGameId = table.Column<int>(type: "int", nullable: true),
                    GameStateJson = table.Column<string>(type: "longtext", maxLength: 128000, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SavedAt = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameState", x => x.GameStateId);
                    table.ForeignKey(
                        name: "FK_GameState_SavedGames_SavedGameId",
                        column: x => x.SavedGameId,
                        principalTable: "SavedGames",
                        principalColumn: "SavedGameId",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_GameState_SavedAt",
                table: "GameState",
                column: "SavedAt");

            migrationBuilder.CreateIndex(
                name: "IX_GameState_SavedGameId",
                table: "GameState",
                column: "SavedGameId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameState");

            migrationBuilder.DropColumn(
                name: "Player1",
                table: "SavedGames");

            migrationBuilder.DropColumn(
                name: "Player2",
                table: "SavedGames");

            migrationBuilder.RenameColumn(
                name: "SavedGameId",
                table: "SavedGames",
                newName: "GameId");

            migrationBuilder.RenameColumn(
                name: "SavedConfigId",
                table: "SavedConfigs",
                newName: "ConfigId");

            migrationBuilder.AlterColumn<string>(
                name: "GameName",
                table: "SavedGames",
                type: "longtext",
                maxLength: 128000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(32)",
                oldMaxLength: 32,
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "GameJson",
                table: "SavedGames",
                type: "longtext",
                maxLength: 128000,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<DateTime>(
                name: "SavedAt",
                table: "SavedGames",
                type: "timestamp(6)",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "SavedAt",
                table: "SavedConfigs",
                type: "timestamp(6)",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");
        }
    }
}
