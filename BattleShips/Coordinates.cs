﻿namespace BattleShips;

public class Coordinates
{
    public int X { get; set; }
    public int Y { get; set; }

    public Coordinates(int x, int y)
    {
        X = x;
        Y = y;
    }

    public override bool Equals(object? obj)
    {
        Coordinates? other = null;
        if (obj != null && obj.GetType() == typeof(Coordinates))
            other = obj as Coordinates;
        
        return other != null && X == other.X && Y == other.Y;
    }
}