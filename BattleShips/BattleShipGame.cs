﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Domain;

namespace BattleShips
{
    // TODO: Initial gamestate gets lost when saving a new game right after creation
    public class BattleShipGame
    {
        public GameRules Rules { get; set; }

        public List<GameState> GameStates = new List<GameState>();
        public GameState? ActiveGameState;
        public bool Finished = false;

        public BattleShipGame() : this(new GameRules()) {}
        
        public BattleShipGame(GameRules rules)
        {
            Rules = rules;
            ActiveGameState = new GameState()
            {
                Player1 = null,
                Player2 = null
            };
            PushGameState(null);
        }

        public BattleShipGame(GameDto game)
        {
            Rules = game.Rules!;
            Finished = game.Finished;
            foreach (var gameStateDto in game.GameStates)
                GameStates.Add(new GameState(gameStateDto));

            ActiveGameState = GameStates.OrderByDescending(x => x.DateTime).First();
        }

        public BattleShipGame(Game game)
        {
            Finished = game.Finished;
            Rules = JsonSerializer.Deserialize<GameRules>(game.GameRuleJson!)!;
            ActiveGameState =
                new GameState(JsonSerializer.Deserialize<GameDto.GameStateDto>(game.GetLatestGameState().GameStateJson)!);
        }

        public void PushGameState(GameState? state)
        {
            // if state is null, then push current active gamestate
            // else this method can be used to set a any gamestate as active
            if (state == null)
            {
                UpdateWinState();
                GameState newState = new GameState(ActiveGameState!);
                GameStates.Add(newState);
                ActiveGameState = newState;
            }
            else
            {
                GameStates.Add(state);
                GameState newState = new GameState(state);
                GameStates.Add(newState);
                ActiveGameState = newState;
            }
        }

        public void UpdateWinState()
        {
            if (ActiveGameState!.Player1 == null || ActiveGameState!.Player2 == null)
                return;
            
            if (ActiveGameState!.Player1!.ShipsIntact() == 0)
            {
                Finished = true;
                ActiveGameState.GameOver = true;
                ActiveGameState.Player2.Winner = true;
            }
            else if (ActiveGameState!.Player2!.ShipsIntact() == 0)
            {
                Finished = true;
                ActiveGameState.GameOver = true;
                ActiveGameState.Player1.Winner = true;
            }
        }
        
        public void SwitchPlayer()
        {
            ActiveGameState!.Activeplayer = !ActiveGameState.Activeplayer;
            PushGameState(null);
        }

        public Player? GetActivePlayer()
        {
            return ActiveGameState!.Activeplayer ? ActiveGameState.Player2 : ActiveGameState.Player1;
        }
        
        public Player? GetEnemyPlayer()
        {
            return ActiveGameState!.Activeplayer ? ActiveGameState.Player1 : ActiveGameState.Player2;
        }

        public void DetonateEnemySquare(Coordinates coord)
        {
            
        if (ActiveGameState!.Activeplayer)
            ActiveGameState.Player1!.DetonateSquare(coord);
        else
            ActiveGameState.Player2!.DetonateSquare(coord);   
        
        SwitchPlayer();
        }

        public GameDto CreateDto()
        {
            List<GameDto.GameStateDto> gameStateDtos = new List<GameDto.GameStateDto>();

            // always add the first, but never the last
            // as the last one is activegamestate (its a copy of the previous state awaiting changes)
            gameStateDtos.Add(GameStates[0].CreateDto());
            for (int i = 1; i < GameStates.Count - 1; i++)
                gameStateDtos.Add(GameStates[i].CreateDto());
            
            GameDto state = new GameDto()
            {
                Rules = Rules,
                GameStates = gameStateDtos,
                Finished = Finished
            };

            return state;
        }
        
        public static bool IsPlacementValid(List<List<BoardSquare>> board, 
                                            EShipTouchRule rule, 
                                            List<Coordinates> coords)
        {
            foreach (var c in coords)
            {
                if ((c.X < 0 || c.X >= board.Count) || (c.Y < 0 || c.Y >= board[0].Count))
                    return false;
                if (rule == EShipTouchRule.Overlapping) continue;
                if (board[c.X][c.Y].Boat) return false;
                if (rule == EShipTouchRule.Side) continue;

                for (int xDelta = -1; xDelta <= 1; xDelta++)
                {
                    if (c.X + xDelta >= board.Count || c.X + xDelta < 0) continue;
                    for (int yDelta = -1; yDelta <= 1; yDelta++)
                    {
                        if (c.Y + yDelta >= board[0].Count || c.Y + yDelta < 0) continue;
                        if (xDelta == 0 || yDelta == 0)
                        {
                            if (board[c.X + xDelta][c.Y + yDelta].Boat)
                                if (rule == EShipTouchRule.None || rule == EShipTouchRule.Corner)
                                    return false;
                        }
                        else
                        {
                            if (board[c.X + xDelta][c.Y + yDelta].Boat)
                                if (rule == EShipTouchRule.None)
                                    return false;
                        }
                    }

                }
            }
            return true;
        }
    }
}