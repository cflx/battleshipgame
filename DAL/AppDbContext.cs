﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public const string ConnectionString = "";

        public DbSet<SavedConfig> SavedConfigs { get; set; } = default!;
        public DbSet<Game> Games { get; set; } = default!;
        public DbSet<GameState> GameState { get; set; } = default!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                // .UseLoggerFactory(MyLoggerFactory)
                // .EnableDetailedErrors()
                // .EnableSensitiveDataLogging()
                .UseMySql(ConnectionString, ServerVersion.AutoDetect(ConnectionString));        
        }

        private static readonly ILoggerFactory MyLoggerFactory =
            LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Debug)
                    .AddFilter("System", LogLevel.Debug)
                    .AddConsole();
            });
    }
}